import { Card, Container, Grid, Image, Text } from '@nextui-org/react'
import React, { useEffect, useState } from 'react'
import { Layout } from '../../components/layouts'
import { FavoritesPokemons } from '../../components/pokemon'
import NoFavorites from '../../components/ui/NoFavorites'
import { localFavorites } from '../../utils'

const Favorites = () => {
  const [favoritePokemon, setFavoritePokemon] = useState<number[]>([])

  useEffect(() => {
    setFavoritePokemon(localFavorites.pokemons)
  }, [])

  return (
    <Layout title="Favoritos">
      {favoritePokemon.length === 0 ?
        <NoFavorites/>
      :
      <FavoritesPokemons pokemons={favoritePokemon} />
    }
    </Layout>
  )
}

export default Favorites