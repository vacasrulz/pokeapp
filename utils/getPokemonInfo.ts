import { pokeApi } from "../api";
import { PokeData } from "../interfaces";

export const getPokemonInfo = async(nameOrId: string) =>{
    const {data} = await pokeApi.get<PokeData>(`pokemon/${nameOrId}`);
    return {
        id: data.id,
        name: data.name,
        sprites: data.sprites,
    }
}