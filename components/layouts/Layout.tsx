import Head from 'next/head'
import React, { FC } from 'react'
import { Navbar } from '../ui';

interface Props {
    children: JSX.Element;
    title?: string;
    image?: string;
}

const Layout: FC<Props> = ({children, title="Pokemon App", image=""}) => {
  return (
    <>
    <Head>
        <title>{title}</title>
        <meta name="description" content={`Información sobre el pokemon ${title}`} />
        <meta name="keywords" content={`${title}, pokemon, pokedex`} />
        <meta property="og:title" content={`Información sobre ${title}}`} />
        <meta property="og:description" content={`Esta es la página de ${title}`}/>
        <meta property="og:image" content={image} />
    </Head>
    <Navbar />
    <main style={{
      padding: '0px 20px'
    }}>{children}</main>    
    </>
  )
}

export {Layout}