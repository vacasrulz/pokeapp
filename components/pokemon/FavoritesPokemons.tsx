import { FC } from 'react'
import { Grid } from '@nextui-org/react'
import {FavoriteCardPokemon} from './';

interface Props {
    pokemons: number[];
}

const FavoritesPokemons: FC<Props> = ({pokemons}) => {
  return (
    <Grid.Container gap={2} direction='row'>
        {pokemons.map((id) => (
            <FavoriteCardPokemon id={id} key={id} />
        ))}
      </Grid.Container>
  )
}

export {FavoritesPokemons}