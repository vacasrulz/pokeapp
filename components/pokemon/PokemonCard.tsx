import React, { FC } from 'react'
import {Text, Card, Grid, Row} from '@nextui-org/react'
import { Pokemon } from '../../interfaces'
import { useRouter } from 'next/router';

interface Props {
    pokemon: Pokemon;
}

const PokemonCard: FC<Props> = ({pokemon}) => {
const router = useRouter()
const onClick = () => {
  router.push(`/name/${pokemon.name}`)
}

  const {id, name, image} = pokemon
  return (
    <Grid xs={6} sm={3} md={2} xl={1} key={id}>
    <Card isHoverable onClick={onClick} isPressable>
        <Card.Body>
        <Card.Image src={image} width="100%" height={140}/>
        </Card.Body>
        <Card.Footer>
        <Row justify='space-between'>
            <Text transform='capitalize'>{name}</Text>
            <Text>#{id}</Text>
        </Row>
        </Card.Footer>
    </Card>
    </Grid>
  )
}

export default PokemonCard